# Fonts
LOCAL_PATH := vendor/minimal/fonts
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,$(LOCAL_PATH)/ttf,$(TARGET_COPY_OUT_PRODUCT)/fonts)

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/fonts_customization.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/fonts_customization.xml

# Fonts required overlays packages
PRODUCT_PACKAGES += \
    FontRobotoCondensed \
    FontBariol \
    FontSamsungOne
