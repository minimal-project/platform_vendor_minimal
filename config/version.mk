# Copyright (C) 2019 Minimal Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Minimal Versioning
MINIMAL_VERSION = v2

ifndef MINIMAL_BUILD_TYPE
    MINIMAL_BUILD_TYPE := BETA
endif

TARGET_PRODUCT_SHORT := $(subst minimal_,,$(MINIMAL_BUILD))

MINIMAL_DATE_YEAR := $(shell date -u +%Y)
MINIMAL_DATE_MONTH := $(shell date -u +%m)
MINIMAL_DATE_DAY := $(shell date -u +%d)
MINIMAL_DATE_HOUR := $(shell date -u +%H)
MINIMAL_DATE_MINUTE := $(shell date -u +%M)
MINIMAL_BUILD_DATE_UTC := $(shell date -d '$(MINIMAL_DATE_YEAR)-$(MINIMAL_DATE_MONTH)-$(MINIMAL_DATE_DAY) $(MINIMAL_DATE_HOUR):$(MINIMAL_DATE_MINUTE) UTC' +%s)
MINIMAL_BUILD_DATE := $(MINIMAL_DATE_YEAR)$(MINIMAL_DATE_MONTH)$(MINIMAL_DATE_DAY)-$(MINIMAL_DATE_HOUR)$(MINIMAL_DATE_MINUTE)
MINIMAL_MOD_VERSION := MinimalProject-$(MINIMAL_VERSION)-$(MINIMAL_BUILD_DATE)-$(MINIMAL_BUILD_TYPE)
MINIMAL_FINGERPRINT := MinimalProject/$(MINIMAL_VERSION)/$(PLATFORM_VERSION)/$(TARGET_PRODUCT_SHORT)/$(MINIMAL_BUILD_DATE)

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
  ro.minimal.version=$(MINIMAL_VERSION) \
  ro.minimal.releasetype=$(MINIMAL_BUILD_TYPE) \
  ro.modversion=$(MINIMAL_MOD_VERSION)

MINIMAL_DISPLAY_VERSION := $(MINIMAL_VERSION)-$(MINIMAL_BUILD_TYPE)

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
  ro.minimal.display.version=$(MINIMAL_DISPLAY_VERSION) \
  ro.minimal.fingerprint=$(MINIMAL_FINGERPRINT) \
  ro.minimal.build_date_utc=$(MINIMAL_BUILD_DATE_UTC)    
