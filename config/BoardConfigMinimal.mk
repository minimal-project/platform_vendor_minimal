include vendor/minimal/config/BoardConfigKernel.mk

ifeq ($(BOARD_USES_QCOM_HARDWARE),true)
include vendor/minimal/config/BoardConfigQcom.mk
endif

include vendor/minimal/config/BoardConfigSoong.mk
